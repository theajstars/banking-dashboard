import Sidebar from './components/Sidebar';
import Topbar from './components/Topbar';
import Dashboard from './components/Dashboard';
import React from 'react';
import './assets/css/fonts.css'
import './assets/css/App.css'
function App() {
  return (
    <>
      <div className="main">
        <Sidebar/>
        <div className="body">
          <Topbar/>
          <Dashboard/>
        </div>
      </div>
    </>
  );
}

export default App;
