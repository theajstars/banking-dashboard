import React, { Component } from 'react'
import '../assets/css/Sidebar.css'

export default class Sidebar extends Component {
    render() {
        return (
            <>
                
                <div className="sidebar">
                    
                    <div className="sidebar-content">
                        <div className="sidebar-logo">
                            <i class="fad fa-alicorn"></i>
                        </div>
                        <div className="sidebar-items">
                            <div className="sidebar-item">
                                <i class="fas fa-credit-card-front"></i> My cards
                            </div>
                            <div className="sidebar-item">
                                <i class="fad fa-envelope-open-dollar"></i> Payments
                            </div>
                            <div className="sidebar-item">
                                <i class="fad fa-gift"></i> Cashback
                            </div>
                            <div className="sidebar-item">
                                <i class="far fa-sync"></i> Exchange
                            </div>
                            <div className="sidebar-item">
                                <i class="fal fa-ballot"></i> All services
                            </div>
                        </div>

                        <div className="settings">
                            <span>
                                <i class="fal fa-cog"></i>&nbsp;Settings
                            </span>
                            <span className="left-icon">
                                <i class="fas fa-chevron-left"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div className="sidebar-small">
                    <div className="sidebar-items-small">
                        <div className="sidebar-logo-small">
                            <i className="fad fa-alicorn"></i>
                        </div>
                        <div className="sidebar-item-small">
                            <i class="fas fa-credit-card-front"></i>
                        </div>
                        <div className="sidebar-item-small">
                            <i class="fad fa-envelope-open-dollar"></i>
                        </div>
                        <div className="sidebar-item-small">
                            <i class="fad fa-gift"></i>
                        </div>
                        <div className="sidebar-item-small">
                            <i class="far fa-sync"></i>
                        </div>
                        <div className="sidebar-item-small">
                            <i class="fal fa-ballot"></i>
                        </div>
                    </div>

                    <div className="settings-small">
                        <span>
                            <i class="fal fa-cog"></i>
                        </span>
                        <span className="right-icon-small">
                            <i class="fas fa-chevron-right"></i>
                        </span>
                    </div>
                </div>
            </>
        )
    }
}
