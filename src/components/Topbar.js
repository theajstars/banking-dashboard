import React, { Component } from 'react'
import '../assets/css/Topbar.css'
import Avatar from '../assets/img/customer-support.svg'

export default class Topbar extends Component {
    render() {
        return (
            <>
                <div className="top-bar">
                    <span className="top-header">
                        You have $23454
                        <span className="cents">.25</span>
                    </span>
                    <span className="menu-down" 
                        onClick={() => {
                            document.querySelector('.menu-actions').style.display = 'flex';
                            document.querySelector('.menu-down').style.display = 'none';
                            document.querySelector('.menu-close').style.display = 'flex';
                        }}
                    >
                        <i class="fas fa-chevron-down"></i>
                    </span>
                    <span className="menu-close" 
                        onClick={() => {
                            document.querySelector('.menu-actions').style.display = 'none';
                            document.querySelector('.menu-down').style.display = 'flex';
                            document.querySelector('.menu-close').style.display = 'none';
                        }}
                    >
                        <i class="far fa-times"></i>
                    </span>
                    <div className="menu-actions">
                        <span className="menu-action menu-search">
                            <i class="far fa-search"></i>
                        </span>
                        <span className="menu-action menu-notification">
                            <i class="fal fa-bell"></i>
                        </span>
                        <span className="menu-action menu-message">
                            <i class="far fa-comment-alt-lines"></i>
                        </span>
                        <span className="menu-action avatar">
                            <i class="far fa-user-tie"></i>
                        </span>
                        
                    </div>
                    
                </div>
            </>
        )
    }
}
