import React, { Component } from 'react'
import '../assets/css/Dashboard.css'
import Chaart from './Chaart'
import BlackFeather from '../assets/img/black-feather.svg'
import BlackGlobe from '../assets/img/black-globe.svg'

export default class Dashboard extends Component {
    render() {
        return (
            <div className="dashboard-container-container">
                <div className="dashboard-container">
                    <div className="card-section">
                        <div className="card">
                            <div className="card-left">
                                <span className="card-header">
                                    Universal
                                </span>
                                <span className="card-amount">
                                    $8564
                                    <span className="cents">.52</span>
                                </span>
                                <div className="card-cvv">
                                    *9532 &nbsp;&nbsp;&nbsp; 04 / 22
                                </div>
                            </div>
                            <div className="card-right">
                                <span className="card-menu-icon">
                                    <i class="far fa-ellipsis-h"></i>
                                </span>
                                <span className="mastercard-logo">
                                    <i class="fab fa-cc-mastercard"></i>
                                </span>
                            </div>
                        </div>
                        <div className="card-details">
                            <div className="card-details-left">
                                <span className="card-detail">Credit Limit</span>
                                <span className="card-detail">Credit Used</span>
                                <span className="card-detail">Currency</span>
                                <span className="card-detail">Status</span>
                            </div>
                            <div className="card-details-right">
                                <span className="card-detail-value">$22000<span className="cents">.00</span></span>
                                <span className="card-detail-value">$0<span className="cents">.00</span></span>
                                <span className="card-detail-value">USD</span>
                                <span className="card-detail-value">Active</span>
                            </div>
                        </div>
                        <div className="tiers">
                            <div className="tier gold">
                                <span>
                                    Gold
                                </span>
                                <span className="tier-price">
                                    $8914<span className="cents">.12</span>
                                </span>
                            </div>
                            <div className="tier junior">
                                <span>
                                    Junior
                                </span>
                                <span className="tier-price">
                                    $500<span className="cents">.25</span>
                                </span>
                            </div>
                            <div className="new-card">
                                <span>
                                    <i class="fas fa-plus"></i> &nbsp; Add new card</span>
                            </div>
                        </div>
                    </div>

                    <div className="transfer-section">
                        <div className="quick-transfer">
                            <span className="head">
                                Quick transfer
                            </span>
                            <div className="transfer-inputs">
                                <input type="text" spellCheck="false" className="card-input" placeholder="Enter the card or phone number" />
                                <input type="text" spellCheck="false" className="amount-input" placeholder="Enter the amount" />
                                <button className="proceed-button">
                                    <i class="fas fa-chevron-right"></i>
                                </button>
                            </div>
                        </div>
                        <div className="money-flow-container">
                            <div className="money-flow">
                                <span className="head">
                                    Money flow
                                </span>
                                <div className="money-flow-filters">
                                    <span className="income">
                                        <i class="dot fas fa-circle"></i>&nbsp;income&nbsp;&nbsp;<i class="down fas fa-angle-down"></i>
                                    </span>
                                    <span className="calendar-filter">
                                        <i class="far fa-calendar-alt"></i>&nbsp;
                                        Jun 01 - Jun 07&nbsp;&nbsp;
                                        <i class="down fas fa-angle-down"></i>
                                    </span>
                                </div>
                            </div>
                            <Chaart/>
                        </div>
                        <div className="transactions">
                            <div className="transactions-top">
                                <div>
                                    <span className="head">Transactions</span> &nbsp; &nbsp; 
                                    <span className="small">June 2021</span>
                                </div>
                                <span className="transactions-menu-icon">
                                    <i class="fad fa-th-large"></i>
                                </span>
                            </div>
                            <div className="transactions-table">
                                <table border="0">
                                    <tr className="table-head">
                                        <td>Name of transaction</td>
                                        <td>Category</td>
                                        <td>Cashback</td>
                                        <td>Amount</td>
                                    </tr>
                                    <tbody>
                                        <tr>
                                            <td className="transaction-name">
                                                <span className="td-icon">
                                                    <img src={BlackFeather} alt="" />
                                                </span>
                                                <div className="td-details">
                                                    <span className="td-head">H&M</span>
                                                    <span className="td-date">Jun 30, 2021 at 19:28</span>
                                                </div>
                                            </td>
                                            <td>
                                                <i class="dot fas fa-circle"></i>&nbsp; Clothes and shoes
                                            </td>
                                            <td className="td-credit">
                                                <i class="fas fa-plus" style={{'fontSize': '12px'}}></i>$25
                                            </td>
                                            <td className="td-debit">
                                                <i class="fas fa-minus" style={{'fontSize': '12px'}}></i>$128<span className="cents">.00</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td className="transaction-name">
                                                <span className="td-icon">
                                                    <img src={BlackGlobe} alt="" />
                                                </span>
                                                <div className="td-details">
                                                    <span className="td-head">McDonald's</span>
                                                    <span className="td-date">Jun 30, 2021 at 19:05</span>
                                                </div>
                                            </td>
                                            <td>
                                                <i class="dot fas fa-circle"></i>&nbsp; Cafe's and restaurants
                                            </td>
                                            <td className="td-credit">
                                                <i class="fas fa-plus" style={{'fontSize': '12px'}}></i>$5
                                            </td>
                                            <td className="td-debit">
                                                <i class="fas fa-minus" style={{'fontSize': '12px'}}></i>$54<span className="cents">.00</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        )
    }
}
